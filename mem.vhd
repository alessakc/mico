-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- UFPR, BCC, ci210 2017-2 trabalho semestral, autor: Roberto Hexsel, 21out
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

use work.p_wires.all;

entity mem_prog is
  port (ender : in  reg6;
        instr : out reg32);

  type t_prog_mem is array (0 to 63) of reg32;

  -- memoria de programa contem somente 64 palavras
  constant program : t_prog_mem := (
    x"00000000", -- nop                  0x00

    x"b0010001", -- addi r1,r0,1         0x01   r1 <- 1
    x"c1000000", -- display r1           0x02

    x"b002fffe", -- addi r2,r0,-2        0x03   r2 <- -2
    x"c2000000", -- display r2           0x04

    x"11230000", -- add r3,r2,r1         0x05   r3 <- 1 + (-2)
    x"c3000000", -- display r3           0x06

    x"32340000", -- mul r4, r2, r3       0x07   r4 <- r2 * r3
    x"c4000000", -- display r5           0x08
    x"34250000", -- mul r5, r4, r2       0x09   r5 <- r4 * r2
    x"c5000000", -- display r5           0x0a

    x"00000000", --                      0x0b
    
    x"90010008", --      r1 <= 8         0x0c   r1 <- zero + 8
    x"c1000000", --      display r1      0x0d
    x"90020001", --      r2 <= 1         0x0e   r2 <- zero + 1
    x"90030001", --      r3 <= 1         0x0f   r3 <- zero + 1
    x"e1000014", -- tst: r1 = r0 ? fim   0x10
    x"31220000", --      r2 <= r1 * r2   0x11
    x"9101ffff", --      r1 <= r1 - 1    0x12
    x"d0000010", --      jump tst        0x13
    x"c2000000", -- fim: display r2      0x14

    x"00000000", -- nop                  0x15

    x"800f4444", -- ori r15, r0, 0x4444  0x16   r15 <- zero OR 0x4444 
    x"b0f00100", -- st  r15, r0, 0x0100  0x17   M[ r1 + 0x100 ] r14 <- 0x4444
    x"cf000000", -- display  r15         0x18

    x"a0080100", -- ld r8, r0, 0x0100    0x19  r8  <- M[ r0 + 0x100 ]
    x"98080001", -- addi r8, r8, 1       0xab  r8 <- r8 + 1
    x"c8000000", -- display  r8          0x1c

    x"00000000", -- nop                  0x1c
    x"00000000", -- nop                  0x1d
    x"00000000", -- nop                  0x1e
     
    x"f0000000", -- halt                 0x1f

    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000");


  function BV2INT6(S: reg6) return integer is
    variable result: integer;
  begin
    for i in S'range loop
      result := result * 2;
      if S(i) = '1' then
        result := result + 1;
      end if;
    end loop;
    return result;
  end BV2INT6;
  
end mem_prog;

-- nao altere esta arquitetura
architecture tabela of mem_prog is
begin  -- tabela

  instr <= program( BV2INT6(ender) );

end tabela;

