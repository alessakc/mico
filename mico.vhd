-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- UFPR, BCC, ci210 2017-2 trabalho semestral, autor: Roberto Hexsel, 21out
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- processador MICO XI
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library ieee; use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.p_wires.all;


entity mico is
  port (rst,clk : in bit);
end mico;

architecture functional of mico is

  component mem_prog is                 -- no arquivo mem.vhd
    port (ender : in  reg6;
          instr : out reg32);
  end component mem_prog;

  component display is                  -- neste arquivo
    port (rst,clk : in bit;
          enable  : in bit;
          data    : in reg32);
  end component display;

  component ULA is                      -- neste arquivo
    port (fun : in reg4;
          alfa,beta : in  reg32;
          gama      : out reg32);
  end component ULA;
 
  component R is                        -- neste arquivo
    port (clk         : in  bit;
          wr_en       : in  bit;
          r_a,r_b,r_c : in  reg4;
          A,B         : out reg32;
          C           : in  reg32);
  end component R;

  component RAM is                      -- neste arquivo
    port (rst, clk : in  bit;
          sel      : in  bit;           -- ativo em 1
          wr       : in  bit;           -- ativo em 1
          ender    : in  reg16;
          data_inp : in  reg32;
          data_out : out reg32);
  end component RAM;

  component mux2 is 
    port (a, b : in bit;
          s    : in bit;
          z    : out bit)


  type t_control_type is record
    extZero    : bit;      -- estende com zero=1, com sinal=0
    selBeta    : bit;      -- seleciona fonte para entrada B da ULA
    wr_display : bit;      -- atualiza display=1
    selNxtIP   : bit;      -- seleciona fonte do incremento do IP
    wr_reg     : bit;      -- atualiza banco de registradores
    selC       : bit;      -- seleciona fonte da escrita no reg destino
    mem_sel    : bit;      -- habilita acesso a RAM
    mem_wr     : bit;      -- habilita escrita na RAM
  end record;

  type t_control_mem is array (0 to 15) of t_control_type;
  
  constant ctrl_table : t_control_mem := (
  --extZ sBeta wrD sIP wrR selC  M_sel M_wr
    ('0','0', '0', '0','0', '0', '0', '0'),            -- NOP
    ('0','0', '0', '0','1', '0', '0', '0'),            -- ADD
    ('0','0', '0', '0','1', '0', '0', '0'),            -- SUB
    ('0','0', '0', '0','1', '0', '0', '0'),            -- MUL
    ('0','0', '0', '0','1', '0', '0', '0'),            -- AND
    ('0','0', '0', '0','1', '0', '0', '0'),            -- OR
    ('0','0', '0', '0','1', '0', '0', '0'),            -- XOR
    ('0','0', '0', '0','1', '0', '0', '0'),            -- NOT
    ('1','1', '0', '0','1', '0', '0', '0'),            -- ORI
    ('0','1', '0', '0','1', '0', '0', '0'),            -- ADDI
    ('0','1', '0', '0','1', '1', '1', '0'),            -- LD
    ('0','1', '0', '0','0', '0', '1', '1'),            -- ST
    ('0','0', '1', '0','0', '0', '0', '0'),            -- SHOW
    ('0','1', '0', '1','0', '0', '0', '0'),            -- JUMP
    ('0','1', '0', '1','0', '0', '0', '0'),            -- BRANCH
    ('0','0', '0', '1','0', '0', '0', '0'));           -- HALT

  constant HALT : bit_vector := x"f";


  signal extZero, selBeta, wr_display, selNxtIP, wr_reg, selC : bit;
  signal mem_sel, mem_wr : bit;

  signal instr, A, B, C, beta, extended, ula_D, mem_D : reg32;
  signal this  : t_control_type;
  signal const, ip, ext_zeros, ext_sinal : reg16;
  signal opcode, r_a, r_b, r_c : reg4;
  signal i_opcode : natural range 0 to 15;
  
begin  -- functional

  -- memoria de programa contem somente 64 palavras
  U_mem_prog: mem_prog port map(ip(5 downto 0), instr);

  opcode <= instr(31 downto 28);
  i_opcode <= BV2INT4(opcode);          -- indice do vetor DEVE ser inteiro
  const    <= instr(15 downto 0);

  -- registradores recebem a instrucao
  r_a <= instr(27 downto 24);
  r_b <= instr(23 downto 20);
  r_c <= instr(19 downto 16);
  const <= instr(15 downto 0);
  
  this <= ctrl_table(i_opcode);         -- sinais de controle

  extZero    <= this.extZero;
  selBeta    <= this.selBeta;
  wr_display <= this.wr_display;
  selNxtIP   <= this.selNxtIP;
  wr_reg     <= this.wr_reg;
  selC       <= this.selC;
  mem_sel    <= this.mem_sel;
  mem_wr     <= this.mem_wr;

  -- extende o const de 16 para 32 bits
  extended <= x"0000" & const(15 downto 0) when instr(15) = '1' and extZero = '1' 
  else x"ffff" & const(15 downto 0);
  
  U_regs: R port map (clk, wr_reg, r_a, r_b, r_c, A, B, C);

 -- entrada do beta
 beta <= B when selBeta = '0'
 else extended;
  
  U_ULA: ULA port map (opcode, A, beta, ula_D);



  --U_mem: RAM port map (rst, clk, , , , gama, );


  
  -- nao altere esta linha
  U_display: display port map (rst, clk, wr_display, A);

  
  assert opcode /= HALT
    report LF & LF & "simulation halted: " & 
    "ender = "&integer'image(BV2INT16(ip))&" = "&BV16HEX(ip)&LF
    severity failure;
  
end functional;
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity ULA is
  port (fun : in reg4;
        alfa,beta : in  reg32;
        gama      : out reg32);
end ULA;

architecture behaviour of ULA is

  -- signal 

begin  -- behaviour




  
  
  
end behaviour;
-- -----------------------------------------------------------------------



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity R is
  port (clk         : in  bit;
        wr_en       : in  bit;          -- ativo em 1
        r_a,r_b,r_c : in  reg4;
        A,B         : out reg32;
        C           : in  reg32);
end R;

architecture rtl of R is

  -- signal

begin






  
end rtl;
-- -----------------------------------------------------------------------


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- display: exibe inteiro na saida padrao do simulador
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use std.textio.all;
use work.p_wires.all;

entity display is
  port (rst,clk : in bit;
        enable  : in bit;
        data    : in reg32);
end display;

architecture functional of display is
  file output : text open write_mode is "STD_OUTPUT";
begin  -- functional

  U_WRITE_OUT: process(clk)
    variable msg : line;
  begin
    if falling_edge(clk) and enable = '1' then
      write ( msg, string'(BV32HEX(data)) );
      writeline( output, msg );
    end if;
  end process U_WRITE_OUT;

end functional;
-- ++ display ++++++++++++++++++++++++++++++++++++++++++++++++++++++++



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- memoria RAM, com capacidade de 64K palavras de 32 bits
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library ieee; use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.p_wires.all;

entity RAM is
  port (rst, clk : in  bit;
        sel      : in  bit;          -- ativo em 1
        wr       : in  bit;          -- ativo em 1
        ender    : in  reg16;
        data_inp : in  reg32;
        data_out : out reg32);

  constant DATA_MEM_SZ : natural := 2**16;
  constant DATA_ADDRS_BITS : natural := log2_ceil(DATA_MEM_SZ);

end RAM;

architecture rtl of RAM is
  
  subtype t_address is unsigned((DATA_ADDRS_BITS - 1) downto 0);
  
  subtype word is bit_vector(31 downto 0);
  type storage_array is
    array (natural range 0 to (DATA_MEM_SZ - 1)) of word;
  signal storage : storage_array;
begin
  
  accessRAM: process(rst, clk, sel, wr, ender, data_inp)
    variable u_addr : t_address;
    variable index, latched : natural;

    variable d : reg32 := (others => '0');
    variable val, i : integer;

  begin

    if (rst = '0') and (sel = '1') then -- normal operation

      index := BV2INT16(ender);

      if  (wr = '1') and rising_edge(clk) then
        
        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramWR index out of bounds: " & natural'image(index)
          severity failure;

        storage(index) <= data_inp;
        
        assert TRUE report "ramWR["& natural'image(index) &"] "
          & BV32HEX(data_inp); -- DEBUG
        
      else

        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramRD index out of bounds: " & natural'image(index)
          severity failure;

        d := storage(index);
        
        assert TRUE report "ramRD["& natural'image(index) &"] "
          & BV32HEX(d);  -- DEBUG

      end if; -- normal operation

      data_out <= d;

    else

      data_out <= (others=>'0');

    end if; -- is reset?
    
  end process accessRAM; -- ---------------------------------------------
  
end rtl;
-- -----------------------------------------------------------------------



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- banco de registradores
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity R is
  port (clk         : in  bit;
        wr_en       : in  bit;          -- ativo em 1
        r_a,r_b,r_c : in  reg4;
        A,B         : out reg32;
        C           : in  reg32);
end R;

architecture rtl of R is
  type reg_file is array(0 to 15) of reg32;
  signal reg_file_A : reg_file;
  signal reg_file_B : reg_file;
  signal int_ra, int_rb, int_rc : integer range 0 to 15;
begin

  int_ra <= BV2INT4(r_a);
  int_rb <= BV2INT4(r_b);
  int_rc <= BV2INT4(r_c);

  A <= reg_file_A( int_ra ) when r_a /= b"0000" else
       x"00000000";                        -- reg0 always zero
  B <= reg_file_B( int_rb ) when r_b /= b"0000" else
       x"00000000";

  WRITE_REG_BANKS: process(clk)
  begin
    if rising_edge(clk) then
      if wr_en = '1' and r_c /= b"0000" then
        reg_file_A( int_rc ) <= C;
        reg_file_B( int_rc ) <= C;
      end if;
    end if;
  end process WRITE_REG_BANKS;
  
end rtl;
-- -----------------------------------------------------------------------


