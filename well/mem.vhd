-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- UFPR, BCC, ci210 2016-2 trabalho semestral, autor: Roberto Hexsel, 07out
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

use work.p_wires.all;

entity mem_prog is
  port (ender : in  reg6;
        instr : out reg32);

  type t_prog_mem is array (0 to 63) of reg32;

  -- memoria de programa contem somente 64 palavras
  constant program : t_prog_mem := (
    x"b0010001",                        -- comeco do fibonacci (constante fib(10))
    x"b0020002",                        	
    x"b003000a",                        
    x"c3000000",                        
    x"10140000",                        
    x"10150000",                        
    x"e300000a",                        --jump pro if (n = 0)
    x"e310000d",                        --jump pro else if (n <= 2)
    x"e320000d",                        --jump pro else if (n <= 2)
    x"d0000010",                        --jump pro else
    x"10060000",
    x"c6000000",
    x"d0000019",                        -- PARA PARAR FIBONACCI: trocar para x"f0000000" (HALT)
    x"10160000",
    x"c6000000",
    x"d0000019",                        -- PARA PARAR FIBONACCI: trocar para x"f0000000" (HALT)
    x"10270000",
    x"e7300017",                        --jump pro fim do for
    x"14560000", 
    x"10450000",
    x"10640000",
    x"17170000",
    x"d0000011",                        --jump pro comeco do for
    x"c6000000",
    x"00000000",                        -- PARA PARAR FIBONACCI: trocar para x"f0000000" (HALT)
    
    x"00000000",                        -- nop

    x"b0080001",                        -- addi r8,r0,1
    x"c8000000",                        -- display r8
    
    -- nao esta demonstrando o resultado a partir daqui por causa do stop-time 
    x"b0090002",                        -- addi r9,r0,2
    x"c9000000",                        -- display r9

    x"189a0000",                        -- add r10,r9,r8
    x"ca000000",                        -- display r10

    x"00000000",                        -- nop
    
    x"289a0000",                        -- sub r10,r9,r8 ---ERRADO
    x"ca000000",                        -- display r10
    
    x"389a0000",                        -- mult r10,r9,r8
    x"ca000000",                        -- display r10
    
    x"489a0000",                        -- and r10,r9,r8
    x"ca000000",                        -- display r10
    
    x"589a0000",                        -- or r10,r9,r8
    x"ca000000",                        -- display r10
    
    x"689a0000",                        -- xor r10,r9,r8
    x"ca000000",                        -- display r10
    
    x"780a0000",                        -- not r10, r9
    x"ca000000",                        -- display r10
    
    x"889a0000",                        -- sll r10,r8,r9
    x"ca000000",                        -- display r10
    
    x"998a0000",                        -- srl r10,r9,r8
    x"ca000000",                        -- display r10
                                
    x"a9a0000f",                        -- ori r10,r9,15
    x"ca000000",                        -- display r10
    x"f0000000",                        -- halt
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",                    
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000");

  function BV2INT6(S: reg6) return integer is
    variable result: integer;
  begin
    if S(5) = '1' then result := -63; else result := 0; end if;
    for i in S'range loop
      result := result * 2;
      if S(i) = '1' then
        result := result + 1;
      end if;
    end loop;
    return result;
  end BV2INT6;
  
end mem_prog;

-- nao altere esta arquitetura
architecture tabela of mem_prog is
begin  -- tabela

  instr <= program( BV2INT6(ender) );

end tabela;

